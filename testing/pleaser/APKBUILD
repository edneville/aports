# Contributor: Ed Neville <ed@s5h.net>
# Maintainer: Ed Neville <ed@s5h.net>
pkgname=please
pkgver=0.4.1
pkgrel=0
pkgdesc="A sudo alternative with regex support"
url="https://gitlab.com/edneville/please"
arch="x86_64 x86 armhf armv7 aarch64 ppc64le" # limited by rust/cargo
license="GPL 3.0"
depends="linux-pam" # Required for password verification
makedepends="cargo linux-pam-dev"
source="https://gitlab.com/edneville/please/-/archive/v$pkgver/please-v$pkgver.tar.gz"
options="suid"
builddir="$srcdir/$pkgname-v$pkgver"

build() {
	cargo build --release --locked
}

check() {
	cargo test --locked
}

package() {
	install -Dm4755 target/release/please "$pkgdir/usr/bin/please"
	install -Dm4755 target/release/pleaseedit "$pkgdir/usr/bin/pleaseedit"
	gzip man/please.1
	gzip man/please.ini.5
	install -Dm644 man/please.1.gz "$pkgdir/usr/share/man/man1/please.1.gz"
	install -Dm644 man/please.1.gz "$pkgdir/usr/share/man/man1/pleaseedit.1.gz"
	install -Dm644 man/please.ini.5.gz "$pkgdir/usr/share/man/man5/please.ini.5.gz"

	mkdir -p "$pkgdir/etc/pam.d"
	for pamf in please pleaseedit; do
		install -Dm0644 "$srcdir/../please.pam" "$pkgdir/etc/pam.d/$pamf"
	done
}

sha512sums="
1880b11698b6372183944442c4922522e2dcea5d8fdd3f85687e80f3ba1139ba6bf7957ba9a59d9b5a0def63dcda66ef4e4667698d329258e749b834e68a39b1  please-v0.4.1.tar.gz
"
